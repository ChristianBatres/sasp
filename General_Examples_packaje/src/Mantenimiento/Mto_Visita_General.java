/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mantenimiento;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author denni
 */
public class Mto_Visita_General {
    Connection cn;
    int codigo_r;
    int codigo_v;
    String nombre_r;
    String nombre_v;
    String fi;
    String ff;
    String descripcion;

    public Mto_Visita_General(){
        Conexion con = new Conexion();
        cn = con.conectar();
    }
    
    public int getCodigo_R() {
        return codigo_r;
    }

    public void setCodigo_R(int codigo_r) {
        this.codigo_r = codigo_r;
    }
    
    public int getCodigo_V() {
        return codigo_v;
    }

    public void setCodigo_V(int codigo_v) {
        this.codigo_v = codigo_v;
    }

    public String getNombre_V() {
        return nombre_v;
    }

    public void setNombre_V(String nombre_v) {
        this.nombre_v = nombre_v;
    }
    
    public String getNombre_R() {
        return nombre_r;
    }

    public void setNombre_R(String nombre_r) {
        this.nombre_r = nombre_r;
    }
    
    public String getFechaI() {
        return fi;
    }

    public void setFechaI(String fi) {
        this.fi = fi;
    }
    
    public String getFechaF() {
        return ff;
    }

    public void setFechaF(String ff) {
        this.ff = ff;
    }
    
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public boolean mthGuardar(){
        boolean resp = false;
        try {
            //Escribiendo consulta
            String sql = "INSERT INTO Visitas_general VALUES (?,?,GETDATE())";
            //Declarando variable cmd y manda la consuta
            PreparedStatement cmd = cn.prepareStatement(sql);
            //Ingreso de los valores
            cmd.setInt(1, codigo_v);
            cmd.setInt(2, codigo_r);
            //validacion de si la consulta se ejecuto satisfactoriamente
            if(!cmd.execute()){
                resp = true;
            }
            //Se cierra la Conexion y el cmd
            cmd.close();
            //cn.close();
        } catch (Exception ex){
            System.out.println("Error: " + ex.getMessage());
        } 
        return resp;
    }
    
    public boolean mthRegistrarBetado(){
        boolean resp = false;
        try {
            //Escribiendo consulta
            String sql = "INSERT INTO Betados VALUES (?, GETDATE(), ?)";
            //Declarando variable cmd y manda la consuta
            PreparedStatement cmd = cn.prepareStatement(sql);
            //Ingreso de los valores
            cmd.setInt(1, codigo_v);
            cmd.setString(2, descripcion);
            //validacion de si la consulta se ejecuto satisfactoriamente
            if(!cmd.execute()){
                resp = true;
            }
            //Se cierra la Conexion y el cmd
            cmd.close();
            //cn.close();
        } catch (Exception ex){
            System.out.println("Error: " + ex.getMessage());
        } 
        return resp;
    }
    
    public ResultSet ConsultaBetados(){
        Statement declara;
        try{
            declara = cn.createStatement();
            ResultSet respuesta = declara.executeQuery("SELECT cod_betado, nombre_visita, apellido_visita, descripcion, CONVERT(VARCHAR(10),fecha,105) FROM Betados AS b, Visitas AS v WHERE b.cod_visita = v.cod_visita");
            return respuesta;
        } catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "ERROR: " + ex);
            return null;
        }
    }
    
    public boolean mthModificar(){
        boolean resp = false;
        try {
            String sql = "UPDATE Visitas SET estado = 12 WHERE cod_visita = ?";
            PreparedStatement cmd = cn.prepareStatement(sql);
            cmd.setInt(1, codigo_v);
            if (!cmd.execute()){
                resp = true;
            } 
            cmd.close();
            //cn.close();
        } catch (Exception ex) {
            System.out.println("Error: " + ex.getMessage());
        }
        return resp;
    }
    
    public ResultSet ConsultaReclusos(){
        Statement declara;
        try{
            declara = cn.createStatement();
            ResultSet respuesta = declara.executeQuery("select cod_recluso, nombre_recluso, apellido_recluso, edad_recluso, estado, nombre_sector, tez, altura, peso, alias, codigo_barra, celda from ((Reclusos as r inner join Estado as e on r.cod_estado = e.cod_estado) inner join Sector as s on r.cod_sector = s.cod_sector inner join Tez as t on r.cod_tez = t.cod_tez) where estado = 'Capturado'");
            return respuesta;
        } catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "ERROR: " + ex);
            return null;
        }
    }
    
    public ResultSet ConsultaVisitas(){
        Statement declara;
        try{
            declara = cn.createStatement();
            ResultSet respuesta = declara.executeQuery("SELECT cod_visita, nombre_visita, apellido_visita, direccion, e.estado FROM Visitas AS v, Estado AS e WHERE v.estado = e.cod_estado AND v.estado = 11");
            return respuesta;
        } catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "ERROR: " + ex);
            return null;
        }
    }
    
    public ResultSet ConsultaRegistrosVisitas(){
        Statement declara;
        try{
            declara = cn.createStatement();
            ResultSet respuesta = declara.executeQuery("SELECT cod_general, nombre_visita, apellido_visita, nombre_recluso, apellido_recluso, celda, nombre_sector, CONVERT(VARCHAR(10),fecha,105) FROM (((Visitas_general AS vg INNER JOIN Visitas AS v ON vg.cod_visita = v.cod_visita) INNER JOIN Reclusos AS r ON vg.cod_recluso = r.cod_recluso) INNER JOIN Sector AS s ON r.cod_sector = s.cod_sector)");
            return respuesta;
        } catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "ERROR: " + ex);
            return null;
        }
    }
    
    public ResultSet FiltrarRegistrosVisitas(){
        Statement declara;
        try{
            declara = cn.createStatement();
            ResultSet respuesta = declara.executeQuery("SELECT cod_general, nombre_visita, apellido_visita, nombre_recluso, apellido_recluso, celda, nombre_sector, fecha FROM (((Visitas_general AS vg INNER JOIN Visitas AS v ON vg.cod_visita = v.cod_visita) INNER JOIN Reclusos AS r ON vg.cod_recluso = r.cod_recluso) INNER JOIN Sector AS s ON r.celda = s.cod_sector) WHERE fecha BETWEEN '"+fi+"' AND '"+ff+"'");
            return respuesta;
        } catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "ERROR: " + ex);
            return null;
        }
    }
    
    public ResultSet FiltrarReclusos(){
        Statement declara;
        try{
            declara = cn.createStatement();
            ResultSet respuesta = declara.executeQuery("select cod_recluso, nombre_recluso, apellido_recluso, edad_recluso, estado, nombre_sector, tez, altura, peso, alias, codigo_barra, celda from ((Reclusos as r inner join Estado as e on r.cod_estado = e.cod_estado) inner join Sector as s on r.cod_sector = s.cod_sector inner join Tez as t on r.cod_tez = t.cod_tez) where estado = 'Capturado' AND nombre_recluso like '"+ nombre_r +"%'");
            return respuesta;
        } catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "ERROR: " + ex);
            return null;
        }
    }
    
    public ResultSet FiltrarVisitas(){
        Statement declara;
        try{
            declara = cn.createStatement();
            ResultSet respuesta = declara.executeQuery("SELECT cod_visita, nombre_visita, apellido_visita, direccion, e.estado FROM Visitas AS v, Estado AS e WHERE v.estado = e.cod_estado AND v.estado = 11 AND nombre_visita like '"+ nombre_v +"%'");
            return respuesta;
        } catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "ERROR: " + ex);
            return null;
        }
    }
    
}
