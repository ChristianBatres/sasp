/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases_Rasq;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.JOptionPane;

/**
 *
 * @author Works
 */
public class Mtod_Registros 
{
 Connection cn;

  
 private int code_usuario;
 private String descripcion;
 Calendar calendario = new GregorianCalendar();
 int año  = calendario.get(Calendar.YEAR);
 int  mes  = calendario.get(Calendar.MONTH)+1;
 int  dia  = calendario.get(Calendar.DAY_OF_MONTH);
 String  fechax;
   


   public int getCode_usuario() {
        return code_usuario;
    }

    public void setCode_usuario(int code_usuario) {
        this.code_usuario = code_usuario;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
 
 public Mtod_Registros()
 {
     Conexion conect = new  Conexion();
 
 cn = conect.conectar();
 
 }
 
   public boolean guardar_registro()
    {
        
         if(mes >0 && mes <=9)
        {
         fechax = "" + año +"-"+"0"+mes+"-"+ dia;
            System.out.println(" " +fechax );
        }
        else
        {
        fechax = "" + año +"-"+""+mes+"-"+ dia;
          System.out.println(" " +fechax);
        }             

    boolean resp = false;
        try {
            String sql = "insert Registros_sistemas(cod_generado,fecha,descripcion) values (?,'"+fechax+"',?)";// insertar datos a la base e datos
            PreparedStatement cmd = cn.prepareStatement(sql);
            //llenar los parametros de la clase
   
             cmd.setInt(1, code_usuario);
             cmd.setString(2,descripcion);
        
            
            
            //si da errror devuelve 1, caso contrario 0
            //Tomar en cuenta el "!" de negacion 
            if(!cmd.execute())
            {
            resp = true;
            }
            cmd.close();
            cn.close();
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(null,ex.toString(),"502 Bad gateway" , JOptionPane.ERROR_MESSAGE);
        }
    
    return  resp; // que retorne de nuevo resp
    }
  
    
    
    
}
